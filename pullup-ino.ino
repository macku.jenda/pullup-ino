//*****************************************************************
//  * buttons debouncing
//  * temperature
//  * battery monitor
//  * control lights
//
//*****************************************************************

#include <LiquidCrystal.h>
#include <Wire.h>
#include <math.h>

///////////////////////////////////
// Dispatcher

enum {
  STATE_START,
  STATE_INIC1,
  STATE_INIC2,
  STATE_DUMMY,
  STATE_WORKOUT 
};

///////////////////////////////////
// Calculation

enum {
  STATE_PRVNI,                                     
  STATE_DRUHEJ,
  STATE_TRETI,
  STATE_CTVRTEJ,
  STATE_PATEJ,
  STATE_SESTEJ,
  STATE_SEDMEJ,
  STATE_OSMEJ,
  STATE_DEVATEJ,
  STATE_VYSLEDKY
};

///////////////////////////////////
// INIC

int borci = 1;
int opak = 1;
int kolo = 1;       // for calculation
int poradi = 1;
int cislokola = 1;  // for calculation total number
char Sborci[3];
char Sopak[3];

///////////////////////////////////
// Print

char sT[3];         // temperature

///////////////////////////////////

struct Osoba {
      int task_cislo;
      int total; 
      int pocet[9];
      char sP[3];
      char sTot[3];
};

///////////////////////////////////
// Temperature

double temperature = 0;
///////////////////////////////////
// State variables

int state_dis = STATE_START;
int state_makej = STATE_PRVNI;
///////////////////////////////////

LiquidCrystal lcd(12, 11, 6, 5, 4, 3);

//up
byte up[8] = {
  B11111,
  B11111,
  B11011,
  B10001,
  B00000,
  B11111,
  B11111,
};

//down
byte down[8] = {
  B11111,
  B11111,
  B00000,
  B10001,
  B11011,
  B11111,
  B11111,
};

//right
byte right[8] = {
  B11111,
  B10111,
  B10011,
  B10001,
  B10011,
  B10111,
  B11111,
};

//left
byte left[8] = {
  B11111,
  B11101,
  B11001,
  B10001,
  B11001,
  B11101,
  B11111,
};

//pause
byte pause[8] = {
  B11111,
  B11111,
  B10101,
  B10101,
  B10101,
  B11111,
  B11111,
};

//heart
byte heart[8] = {
  B00000,
  B01010,
  B11111,
  B11111,
  B11111,
  B01110,
  B00100,
};

/////////////////////////////////////

#define TYPE_CYCLIC 0
#define TYPE_ONDEMAND 1
#define STATE_RUNNING 0
#define STATE_SLEEPING 1


struct Task {
  int iTaskID;                                // task identification
  unsigned long lPeriod;                      // task's period in ms
  unsigned long lLastRun;
  unsigned long lNextRun;                     // last run time stamp in ms
  unsigned long lDuration;                    // time consumed by this task
  byte bType;
  byte bState;
  void (*fnCallBack)(int);   
};

enum {
  TASK_INIC,
  TASK_ZOBRAZENI,
  TASK_VYHODNOCENI,
  TASK_TEPLOTA,
  TASK_TLACITKA,
  TASK_DISPECER,
  TASKS_NUM
};

enum {
  TASK_PRVNI,
  TASK_DRUHY,
  TASK_TRETI,
  TASK_CTVRTY,
  TASKS_IDs
};

struct Task stTasks[TASKS_NUM];
struct Osoba stOsoba[TASKS_IDs];

void setup() 
{
/////////////////////////////////////

  stTasks[TASK_INIC].iTaskID = TASK_INIC;
  stTasks[TASK_INIC].lPeriod = 150;
  stTasks[TASK_INIC].lNextRun = 0;
  stTasks[TASK_INIC].lDuration = 0;
  stTasks[TASK_INIC].bType = TYPE_CYCLIC;
  stTasks[TASK_INIC].bState = STATE_RUNNING;
  stTasks[TASK_INIC].fnCallBack = &fnTask_INIC;

  stTasks[TASK_ZOBRAZENI].iTaskID = TASK_ZOBRAZENI;
  stTasks[TASK_ZOBRAZENI].lPeriod = 500;
  stTasks[TASK_ZOBRAZENI].lNextRun = 50;
  stTasks[TASK_ZOBRAZENI].lDuration = 0;
  stTasks[TASK_ZOBRAZENI].bType = TYPE_CYCLIC;
  stTasks[TASK_ZOBRAZENI].bState = STATE_RUNNING;
  stTasks[TASK_ZOBRAZENI].fnCallBack = &fnTask_ZOBRAZENI;

  stTasks[TASK_VYHODNOCENI].iTaskID = TASK_VYHODNOCENI;
  stTasks[TASK_VYHODNOCENI].lPeriod = 1000;
  stTasks[TASK_VYHODNOCENI].lNextRun = 150;
  stTasks[TASK_VYHODNOCENI].lDuration = 0;
  stTasks[TASK_VYHODNOCENI].bType = TYPE_CYCLIC;
  stTasks[TASK_VYHODNOCENI].bState = STATE_SLEEPING;
  stTasks[TASK_VYHODNOCENI].fnCallBack = &fnTask_VYHODNOCENI;

  stTasks[TASK_TEPLOTA].iTaskID = TASK_TEPLOTA;
  stTasks[TASK_TEPLOTA].lPeriod = 2000;
  stTasks[TASK_TEPLOTA].lNextRun = 1000;
  stTasks[TASK_TEPLOTA].lDuration = 0;
  stTasks[TASK_TEPLOTA].bType = TYPE_CYCLIC;
  stTasks[TASK_TEPLOTA].bState = STATE_SLEEPING;
  stTasks[TASK_TEPLOTA].fnCallBack = &fnTask_TEMPERATURE;

  stTasks[TASK_TLACITKA].iTaskID = TASK_TLACITKA;
  stTasks[TASK_TLACITKA].lPeriod = 300;
  stTasks[TASK_TLACITKA].lNextRun = 50;
  stTasks[TASK_TLACITKA].lDuration = 0;
  stTasks[TASK_TLACITKA].bType = TYPE_CYCLIC;
  stTasks[TASK_TLACITKA].bState = STATE_RUNNING;
  stTasks[TASK_TLACITKA].fnCallBack = &fnTask_TLACITKA;

  stTasks[TASK_DISPECER].iTaskID = TASK_DISPECER;
  stTasks[TASK_DISPECER].lPeriod = 300;
  stTasks[TASK_DISPECER].lNextRun = 50;
  stTasks[TASK_DISPECER].lDuration = 0;
  stTasks[TASK_TLACITKA].bType = TYPE_CYCLIC;
  stTasks[TASK_DISPECER].bState = STATE_RUNNING;
  stTasks[TASK_DISPECER].fnCallBack = &fnTask_DISPECER;

/////////////////////////////////////

  stOsoba[TASK_PRVNI].task_cislo = TASK_PRVNI;
  stOsoba[TASK_DRUHY].task_cislo = TASK_DRUHY;
  stOsoba[TASK_TRETI].task_cislo = TASK_TRETI;
  stOsoba[TASK_CTVRTY].task_cislo = TASK_CTVRTY;
  
  for(int i=0;i<9;i++){
    stOsoba[TASK_PRVNI].pocet[i] = 0;
    stOsoba[TASK_DRUHY].pocet[i] = 0;
    stOsoba[TASK_TRETI].pocet[i] = 0;
    stOsoba[TASK_CTVRTY].pocet[i] = 0;
  }

  stOsoba[TASK_PRVNI].total = 0;
  stOsoba[TASK_DRUHY].total = 0;
  stOsoba[TASK_TRETI].total = 0;
  stOsoba[TASK_CTVRTY].total = 0;
  

/////////////////////////////////////

  pinMode(14, INPUT);
  pinMode(15, INPUT);
 
  lcd.clear();
  
  // declare custom symbols
  lcd.createChar(0, up);
  lcd.createChar(1, down);
  lcd.createChar(2, right);
  lcd.createChar(3, left);
  lcd.createChar(4, pause);
  lcd.createChar(5, heart);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  
}

/////////////////////////////////////

void loop()
{
    fnScheduler();
}
