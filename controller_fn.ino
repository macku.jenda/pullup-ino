void fnTask_DISPECER(int iID)
{
  switch(state_dis)                                  
  { 
    case STATE_START:                                      
      if(digitalRead(16) == LOW) {    
        lcd.clear();      
        state_dis = STATE_INIC1;
      }
      break;

    case STATE_INIC1:
      if((digitalRead(18) == LOW) && (borci<4)){          // right 
        borci +=1;
      }
      else if ((digitalRead(15) == LOW) && (borci>1)){    // left
        borci -=1;
      }
      if ((digitalRead(17) == LOW) && (borci>0)){         // down
        state_dis = STATE_INIC2;
      }
      break;

    case STATE_INIC2:
      if((digitalRead(18) == LOW) && (opak<9)){           // right
        opak +=1;
      }
      else if ((digitalRead(15) == LOW) && (opak>1)){     // left
        opak -=1;
      }
      if ((digitalRead(14) == LOW) && (opak>0)){          // up
        state_dis = STATE_INIC1;
      }      
      if(digitalRead(16) == LOW) {                        // enter
        lcd.clear();
        state_dis = STATE_DUMMY;
      }
      break;

    case STATE_DUMMY:
      for(int i=0;i<9;i++){
        stOsoba[TASK_PRVNI].pocet[i] = 0;
        stOsoba[TASK_DRUHY].pocet[i] = 0;
        stOsoba[TASK_TRETI].pocet[i] = 0;
        stOsoba[TASK_CTVRTY].pocet[i] = 0;
      }

      stOsoba[TASK_PRVNI].total = 0;
      stOsoba[TASK_DRUHY].total = 0;
      stOsoba[TASK_TRETI].total = 0;
      stOsoba[TASK_CTVRTY].total = 0;

      kolo = 1;       
      poradi = 1;
      state_dis = STATE_WORKOUT;
      break;


    case STATE_WORKOUT:
      for(int i=0;i<9;i++){
        stOsoba[TASK_PRVNI].pocet[i] = 0;
        stOsoba[TASK_DRUHY].pocet[i] = 0;
        stOsoba[TASK_TRETI].pocet[i] = 0;
        stOsoba[TASK_CTVRTY].pocet[i] = 0;
      }

      stOsoba[TASK_PRVNI].total = 0;
      stOsoba[TASK_DRUHY].total = 0;
      stOsoba[TASK_TRETI].total = 0;
      stOsoba[TASK_CTVRTY].total = 0;

      kolo = 1;       
      poradi = 1;
      
      stTasks[TASK_TEPLOTA].bState = STATE_RUNNING;
      stTasks[TASK_VYHODNOCENI].bState = STATE_RUNNING;
      stTasks[TASK_INIC].bState = STATE_SLEEPING;
      break;
  

    default:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ERROR DISPEC");                                 
      break;
  }
}
