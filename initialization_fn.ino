// Entery screen and inicialization

void fnTask_INIC(int iID)     
{
  if(state_dis==STATE_START) { 
    lcd.setCursor(0, 0);
    lcd.print("PULL");
    lcd.write(byte(5));
    lcd.print("Upino v0.6.1");
    lcd.setCursor(2, 2);
    lcd.print(">> Press ENTER <<");
  }
  else if((state_dis==STATE_INIC1) || (state_dis==STATE_INIC2)) { 
    lcd.setCursor(1, 0);
    lcd.print("Number of dudes: ");
    lcd.setCursor(1, 1);
    lcd.print("Number of reps: ");
    if(state_dis==STATE_INIC1){
      lcd.setCursor(2, 3);
      lcd.print(">> Press  DOWN <<");
    }
    else if(state_dis==STATE_INIC2){
      lcd.setCursor(2, 3);
      lcd.print(">> Press ENTER <<");
    }
  }
  else {
    lcd.setCursor(18, 0);
    lcd.print((char)223);
    lcd.print("C");
    lcd.setCursor(1, 3);
    lcd.print("Num:");
    lcd.setCursor(8, 3);
    lcd.print("Tot:");
    if ((borci == 2) || (borci == 3) || (borci == 4)){
      lcd.setCursor(1, 2);
      lcd.print("Num:");
      lcd.setCursor(8, 2);
      lcd.print("Tot:");
    }
    if ((borci == 3) || (borci == 4)){
      lcd.setCursor(1, 1);
      lcd.print("Num:");
      lcd.setCursor(8, 1);
      lcd.print("Tot:");
    }
    if (borci == 4){
      lcd.setCursor(1, 0);
      lcd.print("Num:");
      lcd.setCursor(8, 0);
      lcd.print("Tot:");
    }
    
  }
}
